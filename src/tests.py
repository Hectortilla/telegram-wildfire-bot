import unittest

from utils.bisection import bisec, BisecError
from utils.map import deg2tile


class TestBisection(unittest.TestCase):

    def test_bisec_left(self):
        left, right, next_mid, found = bisec(0, 10, True)
        self.assertEqual(left, 0)
        self.assertEqual(right, 5)
        self.assertEqual(next_mid, 2)
        self.assertEqual(found, False)

    def test_bisec_right(self):
        left, right, next_mid, found = bisec(0, 10, False)
        self.assertEqual(left, 5)
        self.assertEqual(right, 10)
        self.assertEqual(next_mid, 7)
        self.assertEqual(found, False)

    def test_bisec_found(self):
        left, right, next_mid, found = bisec(5, 6, True)
        self.assertEqual(left, 5)
        self.assertEqual(right, 5)
        self.assertEqual(next_mid, 5)
        self.assertEqual(found, True)

    def test_bisec_error(self):
        with self.assertRaises(BisecError):
            bisec(10, 0, True)


class TestMapMethods(unittest.TestCase):

    def test_deg2tile(self):
        row, col = deg2tile(-120.70418, 38.32974, 6)
        self.assertEqual(row, 46)
        self.assertEqual(col, 48)


if __name__ == '__main__':
    unittest.main()

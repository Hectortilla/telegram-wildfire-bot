from bernard import layers as lyr
from bernard.conf import settings
from bernard.engine.triggers import BaseTrigger

from src.utils.bisection import bisec
from .store import cs


class Bisection(BaseTrigger):

    def __init__(self, request, found):
        super().__init__(request)
        self.found = found

    def _get_payload(self, key):
        try:
            payload = bool(self.request.get_layer(lyr.Postback).payload[key])
        except (KeyError, ValueError, TypeError):
            return None
        return payload

    def _initialize(self, context):
        go = self._get_payload('go')
        if go is None:
            return 0.0

        context['left'], \
        context['right'], \
        context['image'], \
        context['date'] = settings.ASSETS.get_inital_data()
        return 1.0

    def _bisec(self, context):
        wildfire = self._get_payload('wildfire')
        if wildfire is None:
            return 0.0

        context['left'], context['right'], mid, found = bisec(context['left'], context['right'], wildfire)

        context['date'] = settings.ASSETS.get_date(mid)
        context['image'] = settings.ASSETS.get_image(mid)

        if found == self.found:
            return 1.0

        return 0.0

    @cs.inject()
    async def rank(self, context) -> float:
        if context.get('left') is None:
            return self._initialize(context)

        return self._bisec(context)

# coding: utf-8
from bernard.storage.context import create_context_store


def clear_context(context):
    context['left'] = None
    context['right'] = None
    context['image'] = None
    context['date'] = None


cs = create_context_store(ttl=0)

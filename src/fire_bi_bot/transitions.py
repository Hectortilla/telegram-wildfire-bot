# coding: utf-8

from bernard.engine import Tr
from bernard.engine import triggers as trg
from bernard.i18n import intents as its

from .states import *
from .triggers import *

transitions = [
    Tr(
        dest=S001xWelcome,
        factory=trg.Text.builder(its.HELLO),
    ),
    Tr(
        dest=S002xWildfire,
        origin=S001xWelcome,
        factory=Bisection.builder(found=False)
    ),
    Tr(
        dest=S002xWildfire,
        origin=S002xWildfire,
        factory=Bisection.builder(found=False)
    ),
    Tr(
        dest=S002xWildfire,
        origin=S003xFound,
        factory=Bisection.builder(found=False)
    ),
    Tr(
        dest=S003xFound,
        origin=S002xWildfire,
        factory=Bisection.builder(found=True)
    )
]

# coding: utf-8
from bernard import layers as lyr
from bernard.analytics import page_view
from bernard.engine import BaseState
from bernard.i18n import translate as t
from bernard.platforms.telegram import layers as tgl

from .store import *


class FireBiBotState(BaseState):
    """
    Root class for Fire Bi Bot.

    Here you must implement "error" and "confused" to suit your needs. They
    are the default functions called when something goes wrong. The ERROR and
    CONFUSED texts are defined in `i18n/en/responses.csv`.
    """

    @page_view('/bot/error')
    async def error(self) -> None:
        """
        This happens when something goes wrong (it's the equivalent of the
        HTTP error 500).
        """

        self.send(lyr.Text(t.ERROR))

    @page_view('/bot/confused')
    async def confused(self) -> None:
        """
        This is called when the user sends a message that triggers no
        transitions.
        """

        self.send(lyr.Text(t.CONFUSED))

    async def handle(self) -> None:
        raise NotImplementedError


class S001xWelcome(FireBiBotState):

    @page_view('/bot/welcome')
    @cs.inject()
    async def handle(self, context):
        clear_context(context)

        name = await self.request.user.get_friendly_name()
        self.send(
            lyr.Text(t('WELCOME', name=name)),
            tgl.InlineKeyboard(
                [
                    [
                        tgl.InlineKeyboardCallbackButton(t('GO'), payload={"go": True}),
                    ]
                ]
            ),
        )


class S002xWildfire(FireBiBotState):

    @page_view('/bot/search')
    @cs.inject()
    async def handle(self, context):
        image = context.get('image')
        date = context.get('date')

        self.send(
            tgl.Markdown(t("QUESTION", image=image, date=date)),
            tgl.InlineKeyboard(
                [
                    [
                        tgl.InlineKeyboardCallbackButton(t('YES'), payload={"wildfire": True}),
                        tgl.InlineKeyboardCallbackButton(t('NO'), payload={"wildfire": False}),
                    ]
                ]
            ),
        )


class S003xFound(FireBiBotState):

    @page_view('/bot/found')
    @cs.inject()
    async def handle(self, context):
        image = context.get("image")
        date = context.get("date")

        clear_context(context)

        self.send(
            tgl.Markdown(t("FOUND", image=image, date=date)),
            tgl.InlineKeyboard(
                [
                    [
                        tgl.InlineKeyboardCallbackButton(t('REPEAT'), payload={"go": True}),
                    ]
                ]
            ),
        )

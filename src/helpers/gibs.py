import pendulum
from src.utils.map import deg2tile

# https://wiki.earthdata.nasa.gov/display/GIBS/GIBS+API+for+Developers#GIBSAPIforDevelopers-OGCWebMapTileService(WMTS)
ZOOM = 6
GIBS_WMTS_API = f'https://gibs.earthdata.nasa.gov/wmts/epsg4326/best/MODIS_Terra_CorrectedReflectance_TrueColor/default/<DATE>/250m/{ZOOM}/<ROW>/<COL>.jpg'


class GibsImage:
    def __init__(self, url, cloud_score):
        self.url = url
        if cloud_score:
            self.cloud_score = 0.1  # TODO: GIBS lacks from a cloud score API

    @property
    def image(self):
        return self.url


class GibsAsset:
    def __init__(self, url, date):
        self.url = url
        self.date = date

    def get_asset_image(self, cloud_score=False):
        return GibsImage(self.url, cloud_score)


def assets(lat, lon, begin, end):
    row, col = deg2tile(lat, lon, ZOOM)
    begin, end = pendulum.parse(begin), pendulum.parse(end)
    # A period is iterable
    for dt in end - begin:
        date = dt.format('YYYY-MM-DD')
        url = GIBS_WMTS_API.replace(
            '<DATE>', date
        ).replace(
            '<ROW>', str(row)
        ).replace(
            '<COL>', str(col)
        )

        yield GibsAsset(url, date)

from httpx import Client

from typing import Any, NamedTuple

import os

import pendulum

from . import gibs
from .framex import FrameX

MAX_CLOUD_SCORE = 0.5


class Assets:
    @property
    def count(self):
        raise Exception("Not Implemented")

    def get_date(self, index):
        raise Exception("Not Implemented")

    def get_image(self, index):
        raise Exception("Not Implemented")

    def get_inital_data(self):
        middle = int((self.count - 1) / 2)
        return 0, self.count - 1, self.get_image(middle), self.get_date(middle)


class VideoAssets(Assets):
    """
    Manages the different assets from landsat to facilitate the bisection
    algorithm.
    """

    def __init__(self, name):
        self.api = FrameX()
        self.video = self.api.video(name)
        self._index = 0
        self.image = None

    @property
    def count(self):
        return self.video.frames

    def get_date(self, index):
        return str(index)

    def get_image(self, index):
        return self.api.video_frame(self.video.name, index)


class LandsatAssets(Assets):
    """
    Manages the different assets from landsat to facilitate the bisection
    algorithm.
    """

    def __init__(self, lon, lat):
        self.lon, self.lat = lon, lat
        self.shots = self._get_shots()
        self.index = 0

        # print(f'First = {self.shots[0].asset.date}')
        # print(f'Last = {self.shots[-1].asset.date}')
        # print(f'Count = {len(self.shots)}')

    @property
    def count(self):
        return len(self.shots)

    def get_date(self, index):
        return self.shots[index].date

    def get_image(self, index):
        return self.shots[index].url

    def _get_shots(self):
        """
        Not all returned assets are useful (some have clouds). This function
        does some filtering in order to remove those useless assets and returns
        pre-computed shots which can be used more easily.
        """

        end = pendulum.now('UTC').date().isoformat()
        begin = pendulum.now('UTC').date().subtract(days=31).isoformat()  # This makes the bisec algorith to iterate 5 times 2^5=32

        assets = gibs.assets(lat=self.lat, lon=self.lon, begin=begin, end=end)

        out = []

        for asset in assets:
            img = asset.get_asset_image(cloud_score=True)

            if (img.cloud_score or 1.0) <= MAX_CLOUD_SCORE:
                out.append(asset)

        return out

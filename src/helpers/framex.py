import base64
import os
from typing import List, NamedTuple, Text
from urllib.parse import quote, urljoin

from httpx import Client

API_BASE = os.getenv("API_BASE", "https://framex-dev.wadrid.net/api/")


class Video(NamedTuple):
    """
    That's a video from the API
    """

    name: Text
    width: int
    height: int
    frames: int
    frame_rate: List[int]
    url: Text
    first_frame: Text
    last_frame: Text


class FrameX:
    """
    Utility class to access the FrameX API
    """

    BASE_URL = API_BASE

    def __init__(self):
        self.client = Client()

    def video(self, video: Text) -> Video:
        """
        Fetches information about a video
        """

        r = self.client.get(urljoin(self.BASE_URL, f"video/{quote(video)}/"))
        r.raise_for_status()
        return Video(**r.json())

    def video_frame(self, video: Text, frame: int) -> bytes:
        """
        Fetches the JPEG data of a single frame
        """
        return urljoin(self.BASE_URL, f'video/{quote(video)}/frame/{quote(f"{frame}")}/')

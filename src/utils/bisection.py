class BisecError(Exception):
    pass


def bisec(left, right, answer):
    if left > right:
        raise BisecError()

    mid = int((left + right) / 2)
    if answer:
        right = mid
    else:
        left = mid

    found = False
    if left + 1 >= right:
        found = True

    next_mid = int((left + right) / 2)
    return left, right, next_mid, found

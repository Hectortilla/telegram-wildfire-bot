def deg2tile(lat, lon, zoom):
    # https://github.com/nasa-gibs/onearth/issues/53#issuecomment-299738858
    row = ((90 - lat) * (2 ** zoom)) // 288
    col = ((180 + lon) * (2 ** zoom)) // 288

    return int(row), int(col)

# Fire Bi Bot

The intention of this code is to to satisfy the [With Madrid's recruitment test](https://docs.google.com/document/d/1g-28Wfg5EYFBYcZsB9uJ5Ktt-LeqThYJc2Y7aA0pM6s/edit)

You can talk to this bot at telegram through its name: @FireBiBot

You begin the conversation by greeting ('Hi', 'Hello', 'howdy') or saying 'restart' to @FireBiBot

You also restart the conversation by greeting or saying 'restart' to @FireBiBot

### Requirements

Install **requirement.txt** by running:

``pip install -r requirement.txt``

under your preferred virtual environment.

Set a **Redis** server in your *localhost:6379*

### Run

Rememeber to set the next environment variables:
- BERNARD_SETTINGS_FILE
- BERNARD_BASE_URL
- WEBVIEW_SECRET_KEY
- TELEGRAM_TOKEN

You can run the bot by typing the next command under the root directory of the project:

```make run```

### Code explanation

This project follows the structure described in [BERNARD's documentation](https://github.com/BernardFW/bernard/blob/develop/doc/readme.md)
therefore you will find the expected states/store/transitions/trigger framework under *src/fire_bi_bot*

Under ***src/utils/bisection.py*** you can find the bisection algorithm.

Under ***src/helpers/*** you can find the middleware to retrieve the different datasets: **FrameX** and [**GIBS**](https://wiki.earthdata.nasa.gov/display/GIBS/GIBS+API+for+Developers), this later one built with the intention of replacing the deprecated NASA API of the former [PoC](https://docs.google.com/document/d/10S21ALzRx2pwnwaRp2lPFQZ4r1o5GbkNOdDXvdkHZmI/edit), you can find the my own vesion of this PoC [here](https://gitlab.com/Hectortilla/bisec-poc)

Under ***src/tests.py*** Unit tests to cover the bisection algorithm

The next flow diagram represents the state machine of the bot

![Screenshot](diagram.jpg)

Its possible to make the bot display the GIBS dataset by setting

```
GIBS = True
```

in the settings file: ***src/fire_bi_bot/settings.py***


### Testing

Under the root directory of the project you can simply run the next command to run some tests:

```make test```


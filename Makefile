run: ## Build the docker container
	./manage.py run
test: ## Build the docker container
	python src/tests.py
build-dc: ## Build the docker image
	docker build -t bot -f Docker/Dockerfile .
run-dc: ## Run the docker compose
	docker-compose -f Docker/docker-compose.yml up -d
down-dc: ## shut down the docker containers
	docker-compose -f Docker/docker-compose.yml down
run-image: ## Run the docker image
	docker run -p 5000:5000 bot
